package com.ing.dailybanking.repository;

import com.ing.dailybanking.model.Address;
import com.ing.dailybanking.model.Customer;
import com.ing.dailybanking.repository.AddressRepository;
import com.ing.dailybanking.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ListIterator;
import java.util.Optional;


@RestController
public class APIController {

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private AddressRepository addressRepository;

    @RequestMapping("api/search/{uuid}")
    public Optional<Customer> search(@PathVariable String uuid){
       return repository.findById(uuid);
    }

    @RequestMapping("api/search/all")
    public List<Customer> searchAll(){ return repository.findAll(); }

    @PostMapping("api/create/customer")
    public Customer save(@RequestBody Customer customer){
        return repository.save(customer);
    }

    @PutMapping("api/update/customer")
    public Customer update(@RequestBody Customer customer){
        Customer existingcustomer = repository.findById(customer.getUUID()).orElse(null);
        existingcustomer.setFirstName(customer.getFirstName());
        existingcustomer.setLastName(customer.getLastName());
        existingcustomer.setEmail(customer.getEmail());
        existingcustomer.setTelephoneNumber(customer.getTelephoneNumber());
        existingcustomer.setStatus(customer.getStatus());

        return repository.save(customer);
    }

    @DeleteMapping("api/delete/customer")
    public Customer delete(@RequestBody Customer customer){

        List<Address> addressList = customer.getAddresses();
        ListIterator<Address> addressListIterator = addressList.listIterator();
        while (addressListIterator.hasNext())
        {
            String address_uuid = addressListIterator.next().getAddress_uuid();
            addressRepository.deleteById(address_uuid);
        }
        repository.deleteById(customer.getUUID());
        return customer;
    }
}

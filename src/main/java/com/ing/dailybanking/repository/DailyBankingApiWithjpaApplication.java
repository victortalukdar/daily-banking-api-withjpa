package com.ing.dailybanking.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@EntityScan("com.ing.dailybanking.model")
@SpringBootApplication
public class DailyBankingApiWithjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyBankingApiWithjpaApplication.class, args);
	}

}

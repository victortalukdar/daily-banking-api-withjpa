package com.ing.dailybanking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "address_info")
public class Address {
    // Map all columns with the corresponding address info

    @Id
    @Column(name = "\"address_uuid\"")
    String address_uuid;

    @Column (name = "\"address_line1\"")
    String addressLine1;

    @Column (name = "\"address_line2\"")
    String addressLine2;

    @Column (name = "\"zipcode\"")
    String zipCode;

    @Column (name = "\"city\"")
    String city;

    @Column (name = "\"country\"")
    String country;

    @Column (name = "\"customer_uuid\"")
    String customer_uuid;

    protected Address() {
    }


    public String getUUID() {
        return address_uuid;
    }

    public void setUUID(String address_uuid) {
        this.address_uuid = address_uuid;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress_uuid() {
        return address_uuid;
    }

    public void setAddress_uuid(String address_uuid) {
        this.address_uuid = address_uuid;
    }

    public String getCustomer_uuid() {
        return customer_uuid;
    }

    public void setCustomer_uuid(String customer_uuid) {
        this.customer_uuid = customer_uuid;
    }
}

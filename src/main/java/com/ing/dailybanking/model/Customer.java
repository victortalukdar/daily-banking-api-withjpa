package com.ing.dailybanking.model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "customer_info")
public class Customer {
    // Map all columns with corresponding table columns

    @Id
    @Column (name = "\"uuid\"")
    String UUID;

    @Column (name = "\"first_name\"")
    String firstName;

    @Column (name = "\"last_name\"")
    String lastName;

    @Column (name = "\"email\"")
    String email;

    @Column (name = "\"telephone_number\"")
    String telephoneNumber;

    @Column (name = "\"status\"")
    String status;

    @OneToMany(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn (name = "\"customer_uuid\"")
    List<Address> addresses;

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    protected Customer() {
    }

    public Customer(String FirstName, String LastName) {
        this.firstName = FirstName;
        this.lastName = LastName;
    }

    public Customer(String UUID, String FirstName, String LastName) {
        this.UUID = UUID;
        this.firstName = FirstName;
        this.lastName = LastName;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

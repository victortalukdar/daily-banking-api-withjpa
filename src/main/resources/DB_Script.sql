-- Customer Table --

FORCE CREATE TABLE public.customer_info
(
    first_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    uuid character varying(128) COLLATE pg_catalog."default" NOT NULL,
    email character varying(128) COLLATE pg_catalog."default",
    telephone_number character varying(128) COLLATE pg_catalog."default",
    status character varying(128) COLLATE pg_catalog."default",
    "address_UUID" character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT customer_info_pkey PRIMARY KEY (uuid)
)

TABLESPACE pg_default;

ALTER TABLE public.customer_info
    OWNER to postgres;


-- Address Table --

FORCE CREATE TABLE public.address_info
(
    address_line1 character varying(255) COLLATE pg_catalog."default" NOT NULL,
    address_line character varying(255) COLLATE pg_catalog."default" NOT NULL,
    uuid character varying(128) COLLATE pg_catalog."default" NOT NULL,
    email character varying(128) COLLATE pg_catalog."default",
    zipcode character varying(128) COLLATE pg_catalog."default",
    city character varying(128) COLLATE pg_catalog."default",
    country character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT address_info_pkey PRIMARY KEY ("uuid")
)

TABLESPACE pg_default;

ALTER TABLE public.address_info
    OWNER to postgres;

--- INSERT CUSTOMER RECORDS  ---


INSERT INTO mytable(first_name,last_name,uuid,email,telephone_number,status,address_UUID)
VALUES ('Ronnie','Johnson','05d6bd7f-9dd9-42b8-8095-39531b1071cb','ronnie.johnson@abc.com',+32980852434,'Active',NULL);

INSERT INTO mytable(first_name,last_name,uuid,email,telephone_number,status,address_UUID)
VALUES ('Colin','Richards','d55fe8d8-8315-4919-bc10-881fb763a26d','colin.richards@abc.com',+32375757434,'Active',NULL);

commit;


--- INSERT ADDRESS RECORDS ---



INSERT INTO public.address_info(address_line1,address_line2,uuid,zipcode,city,country)
VALUES ('Avenue Des Nerviens','63','1f494fa3-af64-4661-9d92-46e67d90b418','1040','Brussels','Belgium');

INSERT INTO public.address_info(address_line1,address_line2,uuid,zipcode,city,country)
VALUES ('Rue De La Jonchaie','104','10a5bd9e-7aef-4e28-a997-c02199689de7','1014','Brussels','Belgium')

commit;


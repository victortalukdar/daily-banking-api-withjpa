This is a learning project for me : Victor Talukdar. My end -objective is :

   Phase I : Create a set of APIs which would fetch records from a PostGres DB , and provide response in the form of a JSON object
   
   Phase II : Implement JUNit Test Cases, which will cover the entire gamut of APIs , and check whether there are any errors.
   
   Phase III :CI/CD Implementation with Environment Management : would create a new Environment called PRD, and code will be deployed into PRD only when :
           -- No Errors found on the code
           
           -- All JUnit Test cases pass
           
   Phase IV : Implement Security on my APIs : pass JSON Web Tokens which would be authenticated
   
   Phase V :  Implement a UX based on Node.JS which will use the existing APIs to show a comprehensive UI screen
   
   Phase VI : Learn containerization of the created application using Docker
   
   